import sys
import csv
from elasticsearch import Elasticsearch
from utils import ELASTIC_URL, INDEX

# Elasticsearch client
es = Elasticsearch([ELASTIC_URL])


def csv_uploader(csv_file):
    """**Función encargada de cargar nuevas líneas**

    Manipula los archivos csv y los carga al indice por medio del API de
    elasticsearch. Se espera que la primera columna del archivo csv sea la
    cabecera con el formato l1,l2,variantes,doc_name,pdf_name,document_id

    :param csv_file: Archivo csv con el texto alineado
    :type: File
    :return: Número de líneas cargadas al corpus
    :rtype: int
    """
    total_lines = 0
    with open(csv_file, 'r', encoding='utf-8') as f:
        raw_csv = f.read()
    rows = raw_csv.split('\n')
    # Quitando cabecera del csv
    rows.pop(0)
    total_rows = len(rows)
    for text in csv.reader(rows, delimiter=',', quotechar='"'):
        if text:
            if text[0] and text[1]:
                document = {"pdf_file": text[4],
                            "document_id": text[5],
                            "document_name": text[3],
                            "l1": text[0],
                            "l2": text[1],
                            "variant": text[2]
                            }
                print(f"Subiendo línea {total_lines} de {total_rows}. ", end='')
                res = es.index(index=INDEX, body=document)
                print(f"Status:{res['result']}")
                total_lines += 1
    return total_lines


def main():
    if len(sys.argv) != 1:
        print(f"INDEX: {INDEX}")
        uploaded_lines = csv_uploader(sys.argv[-1])
        print(f"Se subieron {uploaded_lines} líneas al indice {INDEX} :)")
    else:
        print("Ingresa el nombre del archivo CSV")


if __name__ == "__main__":
    main()
