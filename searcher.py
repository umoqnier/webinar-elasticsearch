import sys
from elasticsearch import Elasticsearch
from utils import query_kreator, data_processor, ELASTIC_URL, INDEX
from pprint import pprint

# Elasticsearch client
es = Elasticsearch([ELASTIC_URL])

def search(lang, query, size=10):
    """Se encarga de realizar busquedas en un indice de elasticsearch

    :param lang: lengua en la que se desea buscar
    :type: str
    :param query: La cadena a buscar
    :type: str
    :param size: Total de resultados esperados
    :type: int
    :return: None
    """
    query = f"{lang}:({query})"
    format_query = query_kreator(query, size)
    r = es.search(index=INDEX, body=format_query)
    data_response = r["hits"]
    all_documents = data_response["total"]["value"]
    data = data_processor(data_response, lang, query)
    pprint(data)
    if all_documents >= int(size):
        print(f"Mostrando {size} docs de {all_documents}")


def main():
    if len(sys.argv) != 1:
        search(sys.argv[1], sys.argv[2], sys.argv[3])
    elif len(sys.argv == 3):
        search(sys.argv[1], sys.argv[2])
    else:
        print("Ingresa tu busqueda y cuantos elementos deseas obtener")
        print("$python 'Mexico' 100")


if __name__ == "__main__":
    main()
