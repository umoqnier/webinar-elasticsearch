# Contenido

* ¿Qué es `elasticsearch`?
  * Características
* Caso de uso: Esquite
  * Comunidad Elotl
* Conceptos básicos
* Instalación
  * Inicio y checando funcionamiento
* Índices
  * Obtener información de los índices
  * Creación de índices
* Carga de datos
* Búsquedas
  * Construyendo una query
  * Análisis con *aggregations*
* `python`

# ¿Qué es `elasticsearch`?

* `elasticsearch` es un motor de búsqueda y análisis basado en [Apache Lucene](https://lucene.apache.org/)
* Parte de un *stack* llamado ELK (Elasticsearch, Logstash, Kibana)
* Hecho en `Java`

![Stack ELK](img/elk.png){height=60%}

# Características

* Escalable a *petabytes* de datos estructurados y no estructurados
* Accedemos a el a través de su *RESTful API* y usa el esquema `JSON` para guardar los datos
* Puede ser usado como reemplazo de bases no relacionales como `MongoDB`
* Muchos *Language clients*
	* `Java`. `Python`, `Go`, `Perl`, `.NET`, etc
* *Near Real-Time*
	* Búsquedas rápidas (mas o menos en 1 segundo)
* Full-text search

# Caso de uso: Esquite

![Esquite](img/esquite.png)

# Características de Esquite

* *Framework* escrito en `python` e impulsado por `django` + `elasticsearch`
* Ideal para gestión y búsquedas avanzadas a través de corpus paralelos
* Software Libre :D
* Repositorio: `https://github.com/ElotlMX/Esquite`


# Comunidad Elotl (`https://elotl.mx/`)

![Un elotito :3](img/elotl.png){height=60%}

> Comunidad de entusiastas interesados por el desarrollo e investigación de
tecnologías del lenguaje aplicadas a las lenguas originarias de México.
Nuestras herramientas y recursos digitales son libres y gratuitos. 


# Conceptos básicos


- **Index:** Un index es una colección de documentos que tienen alguna
  característica en común. (Debe estar en minúsculas)

. . . 

* **Type**: Parte de un índice que contiene documentos con campos en común

. . .

- **Documento:** Un documento es la unidad básica de información que puede ser
  indexada. Lod documentos son expresados en *JSON*

. . .

* **Mapping**: Propiedades de los campos, como un tipo de dato. Indica 
  como deberá comportarse el motor con estos campos 

# Comparación entre elasticsearch y un RDBMS

|  Elasticsearch    |  RBMS         |
|:-----------------:|:-------------:|
| Index             | Database      |
| Type              | Table         |
| Document          | Table         |
| Field             | Column        |
| Document          | Row           |


# Instalación

* Vamos a las instrucciones de la [página oficial](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/install-elasticsearch.html)
	* Seguimos los pasos para instalar en nuestro sistema
	* Agregar repositorio
	* Instalar paquete
    * En la configuración esta deshabilitado por defecto para evitar
      actualizaciones cuando se actualiza todo el sistema

. . . 

* Iniciamos `elasticsearch`

```bash
$ sudo systemctl start elasticsearch
$ sudo systemctl status elasticsearch
```

. . . 

* Checamos que este funcionando

```
$ curl localhost:9200
```

# Manos a la obra

![Hackerman](img/hackerman.png)


# Índices

## Información del cluster e índices

* `$curl localhost:9200/_cat/health?v`
* `$curl localhost:9200/_cat/indices`

. . .

## Creación de índices simple

* `$curl -X PUT localhost:9200/<nombre-de-indice>`

. . .

## Creación de índices con configuraciones extras

* `$curl -X PUT -H "Content-Type: application/json" -d @elastic-config.json localhost:9200/mi-indice`

# Configuración de índices

## `settings`

* Configuraciones del indice (hay muchas)
  * Analizador de lenguaje

## `mapping`

* Nos permite definir como nuestros documentos, y sus campos, serán guardados en el índice.
  * Cadenas
  * Números
  * Fechas
  * Geolocalizaciones

# Cargando datos (uno por uno)

* Varias formas de indexar o *ingest* datos a `elasticsearch`
* Al final todas hacen lo mismo: poner objetos `JSON` en `elasticsearch`

```json
PUT /libritos/_doc/1

{
  "nombre": "La llamada de Cthulu", 
  "autor": "H.P. Lovecraft", 
  "genero": "Terror"
}
```

# Cargando datos (De a muchos - *bulk indexing*)

* Significativamente más rápido que enviar *requests* individuales

. . .

* Minimiza el uso de red

. . .

* El tamaño del *batch* óptimo depende del tipo y complejidad de documentos
  * Un bien inicio es *batches* de entre `1000`-`5000` documentos
  * De entre `5MB` a `15MB` de tamaño
* [Datos (click me :p)](https://github.com/elastic/elasticsearch/blob/master/docs/src/test/resources/accounts.json?raw=true)

. . .

## comando

* `$curl -H "Content-Type: application/json" -XPOST "localhost:9200/banquito/_bulk?pretty&refresh" --data-binary "@accounts.json"`

# Búsquedas básicas

* Podemos buscar haciendo *requests* a `/_search`

```json
GET /baquito/_search

{
  "query": { "match_all": {} },
  "sort": [
    { "account_number": "asc" }
  ]
}
```


# ¿Qué es todo esto?

* *took* – Cuanto tiempo le tomo a `elasticsearch` ejecutar la busqueda en milisegundos

. . .

* *timed_out* – Si se agoto el tiempo de espera para hacer la busqueda

. . . 

* *\_shards* – cuantos shards utilizo para la busqueda

. . .

* *max_score* – calificación de los documentos mas relevantes

. . .

* *hits.total.value* - Cuando documentos encontró

. . .

* *hits.sort* - Posición del ordenamiento

. . . 

* *hits.\_score* - Relevancia del documento

# Construyendo una *query*


```json
GET /baquito/_search

{
  "query": { "match": {"address": "mill lane"} }
}

{
  "query": {
    "bool": {
      "must": [
        { "match": { "age": "40" } }
      ],
      "must_not": [
        { "match": { "state": "ID" } }
      ]}}}
```

# Analizando con *aggregations*

```json
GET /banquito/_search
{
  "size": 0,
  "aggs": {
    "group_by_state": {
      "terms": {
        "field": "state.keyword"
      }
    }
  }
}
```

# Utilizando `python`

![python](img/python.png)

# Kibana

![Kibana](img/kibana.jpg)

# Logstash

![Logstash](img/logstash.png)

# Referencias

* [Características de Elasticsearch](https://www.elastic.co/es/elasticsearch/features)
* [Elasticsearch core concepts](https://www.tutorialspoint.com/elasticsearch/index.htm)
* [Guia de instalación oficial](https://www.elastic.co/guide/en/elasticsearch/reference/7.9/rpm.html)
* [Creación de índices](https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html)
  * [Configuración de indices](https://www.elastic.co/guide/en/elasticsearch/reference/current/index-modules.html)
  * [Mapping](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html)
    * [Tipos de datos](https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping-types.html)
  * [Analisis de texto](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis.html)
    * [Analizadores default](https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html)
* [Búsquedas](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-your-data.html)
  * [Query DSL](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl.html)
  * [Query string search](https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-query-string-query.html)
* [Docs del módulo de elasticsearch para python](https://elasticsearch-py.readthedocs.io/en/1.1.1/index.html)
* [Elotl](https://elotl.mx/)
  * [Tsunkua](https://tsunkua.elotl.mx/)
  * [Repo de Esquite](https://github.com/ElotlMX/Esquite)

# Gracias :p

![Eso es todo](img/thatsall.jpg)

---
title: Webinario Elasticsearch
author: Diego A. Barriga (@umoqnier)
institute: Elotl / LIDSOL
theme: metropolis
colortheme: default 
date: "9 de Octubre 2020"
navigation: horizontal
---
