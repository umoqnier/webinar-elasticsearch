ELASTIC_URL = "localhost:9200"
INDEX = "tsunkua"

def query_kreator(query, size):
    """Crea la estructura para busquedas en Elasticsearch

    Función encargada de devolver el objeto con los elementos
    necesarios para realizar una búsqueda en Elasticsearch.
    Esto beneficia la limpieza del código reemplazando una variable
    estática

    :param term: Consulta introducida por la usuaria desde el Frontend
    :type: str
    """
    return {
        "size": size,
        "sort": [{"_score": {"order": "desc"}}],
        "query": {
            "bool": {
                "must": [{
                    "query_string": {
                        "query": query,
                        "analyze_wildcard": True
                    }
                }],
            }
        },
        "highlight": {
            "pre_tags": ["<em>"],
            "post_tags": ["</em>"],
            "fields": {
                "*": {}
            }
        }
    }


def data_processor(raw_data, idioma, query):
    """**Procesa los datos crudo de la API de Elasticsearh para devolver
    solo los resultados**

    Función que recibe una lista con los datos que prove la API de
    ``elacticsearch``, procesa los datos para ignorar los metadatos del API
    y retorna solo los resultados de búsqueda como una lista.

    :param raw_data: Lista de resultados crudos del API de Elasticsearch
    :type: list
    :param idioma: ISO del idioma de búsqueda
    :type: str
    :param query: Cadena de búsqueda
    :type: str
    :return: Resultados de búsqueda
    :rtype: list
    """
    return [highlighter(hit, idioma, query)['_source'] for hit in raw_data['hits']]

"""data = []
    for hit in raw_data['hits']:
        hit = highlighter(hit, idioma, query)
        data.append(hit['_source'])
    return data"""


def highlighter(hit, idioma, query):
    """**Resalta la búsqueda realizada por el usuarix**

    Función que busca el campo ``highlight`` en los objetos devueltos por
    el API de ``elasticsearch`` y lo utilza para reemplazar el texto del
    idioma en el que se realizó la búsqueda. Si la búsqueda es en
    español, el texto dentro del campo highlight estará preprocesado por
    Elasticsearch.

    :param hit: Resultado de búsqueda
    :type: list
    :param idioma: ISO del idioma de búsqueda
    :type: str
    :param query: Cadena de búsqueda
    :type: str
    :return: Texto de los resultados resaltado
    :rtype: list
    """
    if "highlight" in hit:
        for key in hit['highlight'].keys():
            hit['_source'][key] = hit['highlight'][key].pop()
    elif idioma == "l2" and "highlight" not in hit:
        string = hit['_source']['l2']
        # Resaltado manual la lengua 2
        hit['_source']['l2'] = string.replace(query,
                                              '<em>' + query + "</em>")
    return hit
