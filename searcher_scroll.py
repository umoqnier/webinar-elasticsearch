import sys
from elasticsearch import Elasticsearch
from utils import query_kreator, data_processor, ELASTIC_URL, INDEX
from pprint import pprint

# Elasticsearch client
es = Elasticsearch([ELASTIC_URL])

def search(lang, query, sizes=100):
    """Se encarga de realizar busquedas en un indice de elasticsearch
    utilizando la caracteristica de scroll

    :param lang: lengua en la que se desea buscar
    :type: str
    :param query: La cadena a buscar
    :type: str
    :param size: Total de resultados esperados
    :type: int
    :return: None
    """
    query = f"{lang}:({query})"
    format_query = query_kreator(query, sizes)
    r = es.search(index=INDEX, body=format_query, scroll="1m")
    data_response = r["hits"]
    scroll_id = r["_scroll_id"]
    all_documents = data_response["total"]["value"]
    documents_count = len(data_response["hits"])
    while documents_count != all_documents:
        sub_response = es.scroll(scroll_id=scroll_id, scroll="1m")
        data_response["hits"] += sub_response["hits"]["hits"]
        documents_count += len(sub_response["hits"]["hits"])
        scroll_id = sub_response["_scroll_id"]
    data = data_processor(data_response, lang, query)
    pprint(data)


def main():
    if len(sys.argv) != 1:
        search(sys.argv[1], sys.argv[2])
    else:
        print("Ingresa el idioma y la busqueda")
        print("$python l1 'Mexico'")


if __name__ == "__main__":
    main()
