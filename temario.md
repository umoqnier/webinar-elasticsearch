# Webinario Elasticsearch

## Tiempo: 40 a 60 min

## Público objetivo: Desarrolladoras

## Conocimientos básicos

* Comandos GNU/Linux
* `python` básico
	* Entornos virtuales
	* Instalación de paquetes

## Temario

* ¿Qué es `elasticsearch`?
* Casos de uso: Esquite y Comunidad Elotl
* Instalación
  * [Guia de instalción](https://www.elastic.co/guide/en/elasticsearch/reference/current/install-elasticsearch.html)
* Creación de índice
  * Simple
  * Archivo de configuración `json`
    * Campos
    * Analizador de idioma
* Cargando datos
* Haciendo búsquedas básicas
	* API de `elasticsearch`
		* `curl`
		* cliente: Postman
	* Utilizando un lenguaje: `python`
		* Configurando entorno virtual
		* Instalando paquete de `elasticsearch`
		* Creando *scripts*
			* Búsqueda
			* Buckets
			* Paginado
* Mencionando a `elasticsearch` y amigos
	* Kibana
	* Logstash

	
---
title: "Introducción a Elasticsearch"
subtitle: "Webinar Hack por la paz"
date: 9 de Octubre 2020
author: Diego Alberto Barriga Martínez
titlepage: True
---
